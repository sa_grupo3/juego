const socket = io.connect()
var turno = 1;
var habilitado = 0;
var limite = 1000;
socket.on("sala",(res)=> sala=res);


window.setInterval(function(){
	socket.on("desable",(res)=> habilitado=res);
	desabilitar();
	socket.emit("nombre","hola");
	socket.on("nombre1",(res)=> player1.name =res);
	socket.on("nombre2",(res)=>player2.name=res);
	socket.emit("turno","hola");
	socket.on("turno",(res)=>turno=res);
	player1.turn = turno==1;
	player2.turn = turno==2;
	$("#player1-name").text(player1.name);
	$("#player2-name").text(player2.name);
	$("#player1-name").text(player1.name);
	$(".player1-name").text(player1.name);
	if(player1.turn)
	{
		$("#current-name").text(player1.name);
		$("#site-title").css("color", "#AFF584").text(player1.name + " Rolling")
	}
	else{
		$("#current-name").text(player2.name);
		$("#site-title").css("color", "#AFF584").text(player2.name + " Rolling")
	}
	
	
	$(".player2-name").text(player2.name);
	//console.log(player1.name);
	socket.emit("puntaje","hola");
	socket.on("puntaje1",(res)=>player1.score=res);
	socket.on("puntaje2",(res)=>player2.score=res);
	if(lastRound){
		if (player1.score>player2.score) {
			$("#instructions").text("Felicitaciones, " + player1.name + " Gana!!!");
			$("#site-title").css("color", "#AFF584").text("Felicitaciones, " + player1.name + " Gana!!!")
			document.getElementById('bank-button').style.display = 'none';
			document.getElementById('roll-button').style.display = 'none';
			socket.emit("ganador","hola");
		}
		else if (player1.score==player2.score) {
			$("#instructions").text("Felicitaciones, " + player2.name + " wins!!!");
			$("#site-title").css("color", "#AFF584").text("Felicitaciones, " + player2.name + " Gana!!!")
			document.getElementById('bank-button').style.display = 'none';
			document.getElementById('roll-button').style.display = 'none';
			socket.emit("ganador","hola");
		}
		else{
			$("#instructions").text("Felicitaciones, " + player2.name + " wins!!!");
			$("#site-title").css("color", "#AFF584").text("Felicitaciones, " + player2.name + " Gana!!!")
			document.getElementById('bank-button').style.display = 'none';
			document.getElementById('roll-button').style.display = 'none';
			socket.emit("ganador","hola");
		}
		
	}
	else{
		if(player1.score>=limite||player2.score>=limite){
			lastRound = true;
		}
	}
	
	$("#player1-total").text(addCommas(player1.score));
	$("#player2-total").text(addCommas(player2.score));
  }, 1000);


var player1 = {};
var player2 = {};
player1.score = 0;						
player2.score = 0;
var roundScore = 0;
var rollScore = 0;
var tempScore = 0;
player1.name = "Jugador 1";
player2.name = "Jugador 2";
player1.turn = true;
player2.turn = false;
var request = "none";
var diceArray = []; 					
var onePlayerVisited = false;
var lastRound = false;
var youHaveHotDice = false;
var tempRoundScore;
$(document).ready(function() {			
	for (i = 0; i < 6; i++) {	
		diceArray[i] = {};						
		diceArray[i].id = "#die" + (i+1);
		diceArray[i].value = i + 1;
		diceArray[i].state = 0;
	}
});
//Nombres
function getNames() {
	socket.emit("nombre","hola");
	socket.on("nombre1",(res)=>player1.name=res);
	socket.on("nombre2",(res)=>player2.name=res);
	
	
	$("#player1-name").text(player1.name);
	$(".player1-name").text(player1.name);
	$("#current-name").text(player1.name);
	$("#site-title").css("color", "#AFF584").text(player1.name + " Rolling")
	$(".player2-name").text(player2.name);
}

function reloadPage() {
	location.reload(true);
}
function initiateRoll() {
	$(document).ready(function() {
		$("img").off();												
		if (onePlayerVisited === false) {			
				$("#one-player").fadeOut("slow");	
			onePlayerVisited = true;						
		}																			
		request = "roll";											
		gameController();											
	});
}
var dados = "";
function rollDice() {
	//peticion a dados
	
	socket.emit("dados","hola");
	socket.on("dados",(res)=> dados = res); 
	console.log(dados);
			for (var i = 0; i < 6; i++) {																
				if (diceArray[i].state === 0) {															
					$(diceArray[i].id).removeClass("more-faded");							
					diceArray[i].value = dados[i];
				} 																													
		  }
	/*let parametros = {
        
	}
	
	$.ajax({
		url:urldados+'/tirar/6',
		type: 'Get',
		data: parametros,
		success: function(result){
			dados = result.dados
			
		},
		error: function(error){
			console.log(error)
		}
	});*/
}
function updateImage() {
	var dieImage;
	for (var i = 0; i < 6; i++) {
			switch (diceArray[i].value) {						
				case 1: dieImage = "images/1.png";
								break;
				case 2: dieImage = "images/2.png";
								break;
				case 3: dieImage = "images/3.png";
								break;
				case 4: dieImage = "images/4.png";
								break;
				case 5: dieImage = "images/5.png";
								break;
				case 6: dieImage = "images/6.png";
								break;
			}
			$(diceArray[i].id).attr("src", dieImage);
	}
}

function selectDice() {
	$("img").on("click", imageClick);
}
function imageClick() {
	var i = $(this).data("number");			
	if (diceArray[i].state === 0 || diceArray[i].state === 1) {		
		$(this).toggleClass("faded"); 															
		if (diceArray[i].state === 0) {															 
			diceArray[i].state = 1;
		} else {
			diceArray[i].state = 0;
		}
	}
	calculateRollScore();						
	hotDice();
}												
function calculateRollScore() {
	tempScore = 0;
	$("#roll-score").text(addCommas(tempScore));
	var ones = [];
	var twos = [];
	var threes = [];
	var fours = [];
	var fives = [];
	var sixes = [];
	var scoreArray = [];
	for (var i = 0; i < 6; i++) {							
		if (diceArray[i].state === 1) {
			switch (diceArray[i].value) {
				case 1: ones.push(1);
								break;
				case 2: twos.push(2);
								break;
				case 3: threes.push(3);
								break;
				case 4: fours.push(4);
								break;
				case 5: fives.push(5);
								break;
				case 6: sixes.push(6);
								break;
			}
		}
	}
	switch (ones.length) {
		case 1: scoreArray[0] = 100; break;
		case 2: scoreArray[0] = 200; break;
		case 3: scoreArray[0] = 1000; break;
		case 4: scoreArray[0] = 2000; break;
		case 5: scoreArray[0] = 3000; break;
		case 6: scoreArray[0] = 4000; break;
		default: scoreArray[0] = 0;
	}
	switch (twos.length) {
		case 3: scoreArray[1] = 200; break;
		case 4: scoreArray[1] = 400; break;
		case 5: scoreArray[1] = 600; break;
		case 6: scoreArray[1] = 800; break;
		default: scoreArray[1] = 0;
	}
	switch (threes.length) {
		case 3: scoreArray[2] = 300; break;
		case 4: scoreArray[2] = 600; break;
		case 5: scoreArray[2] = 900; break;
		case 6: scoreArray[2] = 1200; break;
		default: scoreArray[2] = 0;
	}
	switch (fours.length) {
		case 3: scoreArray[3] = 400; break;
		case 4: scoreArray[3] = 800; break;
		case 5: scoreArray[3] = 1200; break;
		case 6: scoreArray[3] = 1600; break;
		default: scoreArray[3] = 0;
	}
	switch (fives.length) {
		case 1: scoreArray[4] = 50; break;
		case 2: scoreArray[4] = 100; break;
		case 3: scoreArray[4] = 500; break;
		case 4: scoreArray[4] = 1000; break;
		case 5: scoreArray[4] = 1500; break;
		case 6: scoreArray[4] = 2000; break;
		default: scoreArray[4] = 0;
	}
	switch (sixes.length) {
		case 3: scoreArray[5] = 600; break;
		case 4: scoreArray[5] = 1200; break;
		case 5: scoreArray[5] = 1800; break;
		case 6: scoreArray[5] = 2400; break;
		default: scoreArray[5] = 0;
	}
	tempScore = scoreArray[0] + scoreArray[1] + scoreArray[2] + scoreArray[3] + scoreArray[4] + scoreArray[5];
	$("#roll-score").text(addCommas(tempScore));
	if(player1.turn === true) {
		$("#player1-roll").text(addCommas(tempScore));
		tempRoundScore = roundScore + tempScore;
		$("#player1-round").text(addCommas(tempRoundScore));
	} else {
		$("#player2-roll").text(addCommas(tempScore));
		tempRoundScore = roundScore + tempScore;
		$("#player2-round").text(addCommas(tempRoundScore));
	}
}
function hotDice() {
	var counter = 0;
	for (var i = 0; i < 6; i++) {
		if (diceArray[i].state === -1 || diceArray[i].state === 1) {
			counter++;
		}
	}
	if (counter === 6 && tempScore !== 0) {
		$("#instructions").text("Hot Dice, Tira de nuevo");
		youHaveHotDice = true;
	}
}	

function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function bankScore () {
	$(document).ready(function() {
		$("img").off();		
		request= "bank";											
		gameController();											
	});
}

function checkForWin() {
  
  if (player1.score > player2.score && lastRound === true) {
		$("#instructions").text("Felicitaciones, " + player1.name + " Gana!!!");
		$("#site-title").css("color", "#AFF584").text("Felicitaciones, " + player1.name + " Gana!!!")
  	player1.score = 0;
		player2.score = 0;
		roundScore = 0;
		lastRound = false;;
		socket.emit("ganador","hola");
  }
  if (player2.score > player1.score && lastRound === true) {
		$("#instructions").text("Felicitaciones, " + player2.name + " Gana!!!");
		$("#site-title").css("color", "#AFF584").text("Felicitaciones, " + player2.name + " Gana!!!")
  	player1.score = 0;
		player2.score = 0;
		roundScore = 0;
		lastRound = false;
		socket.emit("ganador","hola");
  }
  //Ganar Aqui
  if (player1.score >= limite && lastRound !== true) {
		$("#instructions").text(player1.name + " Jugador 2 tiene  10,000. " + player2.name + " ultima oportunidad");
  	lastRound = true;
  }
  if (player2.score >= limite && lastRound !== true) {
		$("#instructions").text(player2.name + " jugador 1 tiene 10,000. " + player1.name + "ultima oportunidad");
  	lastRound = true;
  }
}

$(document).ready(function() {									
	var clickableArea = $("<div id='clickable'></div>")	 
	$("body").prepend(clickableArea);
	$("#clickable").css("position", "absolute")
		.css("width","50px")
		.css("height","50px")
		.css("background","transparent")
		.css("cursor","pointer");
	$("#clickable").on("click", function() {
		confetti();
	});
});



function randomPosition(dimension) {
	var position = Math.floor((Math.random() * (dimension - 52)) + 20); 
	return position;
}
//cambiar aqui para separar 
function switchPlayers() {
	socket.emit("puntaje","hola");
	socket.on("puntaje1",(res)=>player1.score=res);
	socket.on("puntaje2",(res)=>player2.score=res);
	/*let parametros ={}
	Url = 'http://localhost:8080/puntajeJ1'
	$.ajax({
        url:Url,
        type: 'Get',
        data: parametros,
        success: function(result){
            player1.score=result
        },
        error: function(error){
            console.log(error)
        }
	});
	setTimeout(() => {
			
	Url = 'http://localhost:8081/puntajeJ2'
	$.ajax({
        url:Url,
        type: 'Get',
        data: parametros,
        success: function(result){
            player2.score= parseInt(result, 10);
        },
        error: function(error){
            console.log(error)
        }
	});
},300);*/
//	setTimeout(() => {
			
	if (player1.turn === true) {
		player1.turn = false;
		player2.turn = true;
		$("#player1-total").text(addCommas(player1.score));
		if (roundScore === 0) {
			$("#player1-round").text("Farkle!!!");
		} else {
			$("#player1-round").text(addCommas(roundScore));
		}
		roundScore = 0;
	} else {
		player1.turn = true;
		player2.turn = false;
		$("#player2-total").text(addCommas(player2.score));
		if (roundScore === 0) {
			$("#player2-round").text("Farkle!!!");
		} else {
			$("#player2-round").text(addCommas(roundScore));
		}
		roundScore = 0;
	}
	//}}, 300);

	if (player1.turn === true) {
		$("#current-name").text(player2.name);
		$("#site-title").css("color", "#AFF584").text(player2.name + " Tirando")
		$("#player2-name").text(player2.name);
	}
	else{
		$("#instructions").text(player1.name + ": start your round by rolling the dice.");
		$("#current-name").text(player1.name);
		$("#site-title").css("color", "#AFF584").text(player1.name + " Tirando");
		$("#player1-name").text(player1.name);
	}
	
	
		
}

function gameController() {
	

	if (request === "roll") {
		rollScore = tempScore;
		//farkle										
		if (rollScore === 0) {										
			roundScore = 0;													
		}
		//limpiar
		roundScore = roundScore + rollScore; 			
		if (player1.turn === true) {
			$("#player1-roll").text("0");
		} else {
			$("#player2-roll").text("0");
		}
		//mostrar la puntuacion total
		if (player1.turn === true) {							
			$("#player1-round").text(addCommas(roundScore));
		} else {
			$("#player2-round").text(addCommas(roundScore));
		}
		//Dados inactivos
		for (var i = 0; i < 6; i++) {							
			if (diceArray[i].state === 1) {
				diceArray[i].state = -1;
				$(diceArray[i].id).removeClass("faded").addClass("more-faded");
			}
		}
		//reseteo por HotDice
		if (youHaveHotDice === true) {						
			for (i = 0; i < 6; i++) {
				diceArray[i].state = 0;
			}
		}	

		tempScore = 0;
		$("#roll-score").text(addCommas(tempScore));
		rollDice();		
		setTimeout(() => { updateImage();	 }, 300);													
										 					
		if (youHaveHotDice === true) {
			$("#instructions").text("You have Hot Dice! Keep rolling or bank your score.");
		} else {
			$("#instructions").text("Select scoring dice, then Roll or Bank.");
		}
		youHaveHotDice = false;		
		selectDice();		
	}
	if (request === "bank") {
		//total
		socket.emit("desable","disable");
		habilitado=0;
		socket.emit("puntaje","hola");
		socket.on("puntaje1",(res)=>player1.score=res);
		socket.on("puntaje2",(res)=>player2.score=res);
		if(player1.turn){
			socket.emit("aumentar1",tempRoundScore);
		}
		else{
			socket.emit("aumentar2",tempRoundScore);
		}
		socket.on("puntaje1",(res)=>player1.score=res);
		socket.on("puntaje2",(res)=>player2.score=res);
		socket.emit("Cambio:turno","");

		/*Url = (player1.turn)?'http://localhost:8080/jugada':'http://localhost:8081/jugada'
		
	let parametros = {
		turno: (player1.turn)?1:2,
		puntaje: tempScore
		
    }
	$.ajax({
        url:Url,
        type: 'Post',
        data: parametros,
        success: function(result){
        },
        error: function(error){
            console.log(error)
        }
	});
	*/
	
		//setTimeout(() => {
			rollScore = tempScore;	
			if (rollScore === 0) {										
				roundScore = 0;											
			}	
			roundScore = roundScore + rollScore; 
				tempScore = 0;													
				$("#roll-score").text(addCommas(tempScore));		
							
				for (var i = 0; i < 6; i++) {							
					diceArray[i].state = 0;
					$(diceArray[i].id).removeClass("faded").removeClass("more-faded").addClass("more-faded");
				}
		switchPlayers();
		checkForWin();	
		//}, 300);	
	}
	
}
function desabilitar(){
	//console.log(habilitado+" habilitado es");
	if (habilitado==0) {
		document.getElementById('bank-button').style.display = 'none';
		document.getElementById('roll-button').style.display = 'none';
	}
	else {
		document.getElementById('bank-button').style.display = 'inline';
		document.getElementById('roll-button').style.display = 'inline';
	}
}
