var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Usuario = new Schema({
    id: {
        type: Number
    },
    nombres: {
        type: String,
        default: ''
    },
    apellidos: {
        type: String,
        default: ''
    },
    administrador: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Usuario', Usuario);
