const request = require("supertest");
const app = require("../app");
let server, agent;

// Se inicia el servidor para comenzar a escuchar peticiones
beforeEach((done) => {
  server = app.listen(8000, (err) => {
    if (err) return done(err);
    agent = request.agent(server); // se obtiene el puerto en el que corre el server
    done();
  });
});
// despues de cada prueba se cierra el servidor iniciado en beforeEach
afterEach((done) => {
  return server && server.close(done);
});
// Server
describe("Pruebas solicitar de simular", function () {
  test("post /generar", function (done) {
    request(app)
      .post("/generar")
      .set({ "Content-Type": "application/json" })
      .send({
        id:"ZXCV-LKJASDJ-23-1DSF",
        jugadores:[1,5]
      })
      .expect(200)
      .end(function (err, res) {
        if (err) return done(err);
        done();
      });
  });
});