var express = require('express')
var app = express()
var cors = require('cors')
var bodyParser = require("body-parser")
var logger = require('morgan');
const path = require('path');
const SocketIO = require('socket.io');
const axios = require('axios');
const jwt = require('jsonwebtoken');
var publicKey=process.env.PUBLIC_KEY||"-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCAQA5vqqDW0VcY791GwD5NzII5DPO9cWQfDGu6G/nUUkOFbef1KP9t7MCbqE/fpry5bBnkeaxWp4VzC2Nck0DQ4IuWUC3Uhz9VewCheYgCY/pSGaJoMYJyLk6Bjh98knJ1GtylHsJ+N8JlnVg4FLqyYSFdbV5YGz82iREAYseRMwIDAQAB\n-----END PUBLIC KEY-----"
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors())
app.use(logger('dev'));


let token = {}
let token2 = {}


let turno = 1;
let bankJ1 = 0;
let bankJ2 = 0;
let salaDisponible = "";
const dotenv = require('dotenv');
dotenv.config();
app.use(express.static(path.join(__dirname,'public')));
let puerto =  process.env.PORT || 8000;
let dados = "http://"+process.env.DADOS_HOST || "http://18.206.38.147:3001";
let torneos = "http://"+process.env.TORNEOS_HOST || "http://18.206.38.147:3002";
let usuario = "http://"+process.env.USUARIOS_HOST || "http://18.206.38.147:5000";
let urlDeJuego = "http://"+process.env.JUEGO_HOST || "http://18.206.38.147:8000";
app.set('port', puerto);
const server = require('http').createServer(app)
const io = require('socket.io')(server)
var partidas = [];
var salas = [];
var datos = [];
io.on('connection', (socket)=>{
	id = socket.id;
	salas[id] = salaDisponible;
	if (datos[salaDisponible]==undefined) {
		datos[salaDisponible]={puntos1:0,puntos2:0,turno:1,almacenado:0,id1:'',id2:'',"jugador1":"Jugador1","jugador2":"Jugador2"};	
	}
	else{
		io.to(salas[socket.id+""]).emit("desable",1);
	}
	
	console.log(datos);
	socket.join(salaDisponible);
	socket.on('prueba',(data)=>{
	});
	socket.on('urlJuego',(data)=>{
		//console.log(data);
		io.to(socket.id).emit("urlJuego",urlDeJuego);
	})
	socket.on('dados',async (data)=>{
		var tk = await GetToken();
		var config = {
		   headers: {
			   'Content-Type': 'application/json',
			   Authorization: "Bearer "+ tk.jwt
		   }
		};
	respt =await axios.get( dados + "/tirar/6",config);
	console.log(respt);
		io.to(socket.id).emit("dados",respt.data.dados);
	})
	socket.on('puntaje',(data)=>{
		//console.log(data);
		io.in(salas[socket.id+""]).emit("puntaje1",datos[salas[socket.id+""]].puntos1);
		io.in(salas[socket.id+""]).emit("puntaje2",datos[salas[socket.id+""]].puntos2);
	})
	socket.on('nombre',(data)=>{
		//console.log(data);
		//console.log(salas);
		io.in(salas[socket.id+""]).emit("nombre1",datos[salas[socket.id+""]].jugador1);
		io.in(salas[socket.id+""]).emit("nombre2",datos[salas[socket.id+""]].jugador2);
	})
	socket.on('turno',(data)=>{
		//console.log(data);
		//console.log(salas);
		io.in(salas[socket.id+""]).emit("turno",datos[salas[socket.id+""]].turno);
	})
	socket.on('Cambio:turno',(data)=>{
		turno = datos[salas[socket.id+""]].turno;
		datos[salas[socket.id+""]].turno = (turno==1)?2:1; 
		io.in(salas[socket.id+""]).emit("turno",datos[salas[socket.id+""]].turno);
	})
	socket.on('desable',(data)=>{
		io.to(salas[socket.id+""]).emit("desable",1);
		io.to(socket.id).emit("desable",0);
	})
	socket.on('aumentar1',(data)=>{
		datos[salas[socket.id+""]].puntos1 += data; 
		io.in(salas[socket.id+""]).emit("puntaje1",datos[salas[socket.id+""]].puntos1);
		io.in(salas[socket.id+""]).emit("puntaje2",datos[salas[socket.id+""]].puntos2);
	})
	socket.on('aumentar2',(data)=>{
		datos[salas[socket.id+""]].puntos2 += data; 
		io.in(salas[socket.id+""]).emit("puntaje1",datos[salas[socket.id+""]].puntos1);
		io.in(salas[socket.id+""]).emit("puntaje2",datos[salas[socket.id+""]].puntos2);
	})
	socket.on('ganador',async (data)=>{
		var tk = await GetToken();
		var config = {
		   headers: {
			   'Content-Type': 'application/json',
			   Authorization: `Bearer ${tk.jwt}`
		   }
		};
	axios.put( torneos + "/partidas/"+salas[socket.id+""]+"/", { marcador:[datos[salas[socket.id+""]].puntos1,datos[salas[socket.id+""]].puntos2]},config);
	})
	
});
//web socket

app.get('/juego/:servidor/', function (req, res) {
	salaDisponible  =  req.params.servidor;
	 //res.sendFile(__dirname, 'public');
		res.redirect('/');
  });



app.get('/puntajeJ2', function (req, res) {
	var user = new Users({ id:1,nombres: "David", apellidos:"Alvarado"  });
 
    // save model to database
    user.save(function (err, user) {
      if (err) return console.error(err);
      console.log(user.nombres + " saved to bookstore collection.");
    });
	res.send(''+bankJ2);
	//generar partida
  });
app.get('/puntajeJ1', function (req, res) {
	var partida = new Partida({ id:1,jugadores: [1,2], marcador: [0,0] });
 
    // save model to database
    partida.save(function (err, book) {
      if (err) return console.error(err);
      console.log(book.name + " saved to bookstore collection.");
    });
  res.send(''+bankJ1);
  //generar partida
});

app.post('/jugada', function (req, res) {


    bankJ1 +=parseInt(req.body.puntaje, 10);
    console.log(bankJ1)
  res.send();
  //generar partida
});

app.post('/jugada2', function (req, res) {


    bankJ1 +=parseInt(req.body.puntaje, 10);
    console.log(bankJ1)
  res.send();
  //generar partida
});
/*
async function GetToken() {
	var token_url = process.env.TOKEN_HOST || "http://18.206.38.147:5001";
  
	var token3 = await axios.post(`${token_url}/token?id=craps&secret=manager1`, {})
	  .then(function (response) {
		var tk = response.data
		return tk;
  })
  .catch(function (error) {
	console.log(error);
  });
	return token3;
  }
*/  

async function GetToken() {
	var token_url = process.env.TOKEN_HOST || "http://18.206.38.147:5001";
   var config = {
	method: 'post',
	url: `http://${token_url}/token?id=craps&secret=manager1`,
	headers: { }
  };
  token = await axios(config)
  .then(function (response) {
	token2 = response.data;
	//console.log(token2);
	return token2;
  })    
  .catch(function (error) {
	console.log(error);
  });
  return token;
  }
  


/*
{
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "jugadores": [
    0
  ]
}

*/
app.get('/jugador1', function (req, res) {
  res.send('David');
  //generar partida
});
app.get('/jugador2', function (req, res) {
	res.send('Andres');
	//generar partida
  });

app.post('/generar', function (req, res) {
	const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }

    const verifiacion = jwt.verify(token,publicKey,verifyOptions,function(err, verifiacion){
        if(err){
            res.status(400).send({message: "Token invalido"})
        }else{
            partidas.push(req.body)
			res.status(200).send("1");
        }
    })

	
    //generar partida
  });

  app.get('/usuario/:id/', function (req, res) {
	
	var identificador = req.params.id;
	console.log(identificador);
	console.log(partidas);
	var respuesta = partidas.filter(sala=>sala.jugadores[0]==identificador||sala.jugadores[1]==identificador)
	res.status(200).send(respuesta);
    //generar partida
  });

  app.post('/login',async function (req, res, next) {
	  console.log(usuario);
	  var tk = await GetToken();
	  console.log(tk.jwt);
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }

	 };
	 
	//const resp = await axios.get(usuario + "/login?email="+req.body.email+"&password="+req.body.password,config)
	const resp = await axios.get(usuario + "/login?email="+req.body.email+"&password="+req.body.password ,config);
	/*.then(res2 => {
		console.log(res2);
		if (res2.status == 201||res2.status==200) { 
			res.status(200).send(resp.data);
		} else if (res2.statusCode == 406) {
			res.status(400).send("Datos Invalidos");
			status = 400;
		}
	})
	.catch(e => res.status(406).send("Datos Invalidos"));*/
	if (resp.status == 200) {
		console.log("Ingreso exitoso");
		res.status(200).send(resp.data);
	  } else {
		res.sendStatus(400);
	  }
	
  });

  /*
  {
  "id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
  "jugadores": [
    0,0
  ]
}
*/
app.post('/simular',async function (req, res) {
	const bearerHeader = req.headers['authorization'];
    const token = bearerHeader.split(" ")[1]
    var verifyOptions = {
        algorithm:"RS256"
    }
    if(!token){
        return res.status(400).send({message: "usuario o secret invalido"})
    }

    const verifiacion = jwt.verify(token,publicKey,verifyOptions,function(err, verifiacion){
        if(err){
            res.status(400).send({message: "Token invalido"})
        }
    })
	var id = req.body.id;
	var P1;
	var P2;
	/*
	Partida.findById(req.body.jugadores[0], (err, partida) => {
        if (err){res.status(404).send(`Error  jugador no existente : ${err}`)}//500 Datos invalidos
        if (!partida){ res.status(406).send(`Parametro no valido: ${err}`)}
        P1 = partida;
	})
	Partida.findById(req.body.jugadores[1], (err, partida) => {
        if (err){ res.status(404).send(`Error  jugador no existente : ${err}`)}//500 Datos invalidos
        if (!partida){ res.status(406).send(`Parametro no valido: ${err}`)}
        P2 = partida;
    })*/

	var jugador1 = 0;
	var jugador2 = 0;
	var turno = 1;
	while(true)
	{
	var ones = [];
	var twos = [];
	var threes = [];
	var fours = [];
	var fives = [];
	var sixes = [];
	var scoreArray = [];
	var dados = [0,0,0,0,0,0,0]
	for (var i = 0; i < 6; i++) {
		dados[i]=  Math.round(Math.random() * (6 - 1) + 1);
	}	
	for (var i = 0; i < 6; i++) {	
			switch (dados[i]) {
				case 1: ones.push(1);
								break;
				case 2: twos.push(2);
								break;
				case 3: threes.push(3);
								break;
				case 4: fours.push(4);
								break;
				case 5: fives.push(5);
								break;
				case 6: sixes.push(6);
								break;
			}
	}
	switch (ones.length) {
		case 1: scoreArray[0] = 100; break;
		case 2: scoreArray[0] = 200; break;
		case 3: scoreArray[0] = 1000; break;
		case 4: scoreArray[0] = 2000; break;
		case 5: scoreArray[0] = 3000; break;
		case 6: scoreArray[0] = 4000; break;
		default: scoreArray[0] = 0;
	}
	switch (twos.length) {
		case 3: scoreArray[1] = 200; break;
		case 4: scoreArray[1] = 400; break;
		case 5: scoreArray[1] = 600; break;
		case 6: scoreArray[1] = 800; break;
		default: scoreArray[1] = 0;
	}
	switch (threes.length) {
		case 3: scoreArray[2] = 300; break;
		case 4: scoreArray[2] = 600; break;
		case 5: scoreArray[2] = 900; break;
		case 6: scoreArray[2] = 1200; break;
		default: scoreArray[2] = 0;
	}
	switch (fours.length) {
		case 3: scoreArray[3] = 400; break;
		case 4: scoreArray[3] = 800; break;
		case 5: scoreArray[3] = 1200; break;
		case 6: scoreArray[3] = 1600; break;
		default: scoreArray[3] = 0;
	}
	switch (fives.length) {
		case 1: scoreArray[4] = 50; break;
		case 2: scoreArray[4] = 100; break;
		case 3: scoreArray[4] = 500; break;
		case 4: scoreArray[4] = 1000; break;
		case 5: scoreArray[4] = 1500; break;
		case 6: scoreArray[4] = 2000; break;
		default: scoreArray[4] = 0;
	}
	switch (sixes.length) {
		case 3: scoreArray[5] = 600; break;
		case 4: scoreArray[5] = 1200; break;
		case 5: scoreArray[5] = 1800; break;
		case 6: scoreArray[5] = 2400; break;
		default: scoreArray[5] = 0;
	}
	
	var total = scoreArray[0] + scoreArray[1] + scoreArray[2] + scoreArray[3] + scoreArray[4] + scoreArray[5];
	if(turno==1){
		console.log('Puntos Obtenidos por Jugador 1: '+total)
		jugador1 += total;
		console.log('Puntos Totales: '+jugador1);
		turno = 2;
		if(jugador1>=10000)break;
	}
	else{
		console.log('Puntos Obtenidos por Jugador 2: '+total)
		jugador2+= total;
		console.log('Puntos Totales: '+jugador2);
		turno = 1;
		if(jugador2>=10000)break;
	}
}
	//res.status(200).json({id:id,marcador:[jugador1,jugador2]});
	console.log('Restado: ');
	console.log('Jugador1: '+jugador1+' Jugador2: '+jugador2);
	console.log(torneos + "/partidas/"+id);
	 var tk = await GetToken();
     var config = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${tk.jwt}`
        }
     };

const resp = await axios.put(torneos+ "/partidas/"+id+"/", {  marcador:[jugador1,jugador2]  },config);

	res.sendStatus(201);
	
  });

 module.exports=server;
